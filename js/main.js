
/* owl-carousel plugin activation*/

/* owl-carousel for hot deal product*/
$(document).ready(function(){
  $(".owl-carousel.hot-deal-product").owlCarousel({
     items:1,
     margin:20,
     loop:true,
     autoplay:true,
     autoplayTimeout:1000,
     autoplayHoverPause:true,
     responsiveClass:true,
     nav:true,
     navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  });
});


$(document).ready(function(){
  $(".owl-carousel.product-list").owlCarousel({
  	 items:4,
  	 margin:20,
  	 loop:true,
  	 autoplay:true,
     autoplayTimeout:1000,
     autoplayHoverPause:true,
     responsiveClass:true,
     nav:true,
      navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            
        }
    }
  });
});

